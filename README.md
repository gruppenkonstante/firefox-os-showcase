# Firefox OS Showcase #
----

## Requirements ##
Firefox OS 2.2

## Installation ##
1.  npm install
2.  bower install
3.  grunt

## Run the App ##
Go to the Firefox WebIDE and load the Firefox_Debug Folder.

## Develop ##
Choose your IDE and work in the app folder. The grunt watch service compile it to the Firefox_Debug folder automatically.

## Compile ##
grunt build-job