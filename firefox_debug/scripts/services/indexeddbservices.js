'use strict';

/**
 * @ngdoc function
 * @name firefoxOSShowcase.service:indexedDBService
 * @description
 * je nach funktion wird getPersonen, addPerson, oder deletePerson zurückgegeben
 * # indexedDBService
 * Service of the firefoxOSShowcase
 */

app.factory('indexedDBService', function($window, $q){

  var indexedDB = $window.indexedDB;
  var db=null;
  var lastIndex=0;
  
  var open = function(){
    var deferred = $q.defer();
    var version = 1;
    var request = indexedDB.open('personenDB', version);





 request.onupgradeneeded = function(e) {
      db = e.target.result;
  
      e.target.transaction.onerror = indexedDB.onerror;
  
      if(db.objectStoreNames.contains('person')) {
        db.deleteObjectStore('person');
      }
  
      db.createObjectStore('person',
        {keyPath: 'id'});
    };
   
  
    request.onsuccess = function(e) {
      db = e.target.result;
      deferred.resolve();
    };
  
    request.onerror = function(){
      deferred.reject();
    };
    
    return deferred.promise;
  };
  
  ///////////////////////// get ///////////////////////// 
  var getPersonen = function(){
    var deferred = $q.defer();
    
    if(db === null){
      deferred.reject('IndexDB wurde nicht geöffnet.');
    }
    else{
      var trans = db.transaction(['person'], 'readwrite');
      var store = trans.objectStore('person');
      var Personen = [];
    
      // Alle Personen;
      var keyRange = IDBKeyRange.lowerBound(0);
      var cursorRequest = store.openCursor(keyRange);
    
      cursorRequest.onsuccess = function(e) {
        var result = e.target.result;
        if(result === null || result === undefined)
        {
          deferred.resolve(Personen);
        }
        else{
          Personen.push(result.value);
          if(result.value.id > lastIndex){
            lastIndex=result.value.id;
          }
          result.continue();
        }
      };
    
      cursorRequest.onerror = function(e){
        console.log(e.value);
        deferred.reject('Argh!!');
      };
    }
    
    return deferred.promise;
  };
  

  /////////////////////////  delete ///////////////////////// 
  var deletePerson = function(id){
    var deferred = $q.defer();
    
    if(db === null){
      deferred.reject('IndexDB ist wurde nicht geöffnet.');
    }
    else{
      var trans = db.transaction(['person'], 'readwrite');
      var store = trans.objectStore('person');
    
      var request = store.delete(id);
    
      request.onsuccess = function() {
        deferred.resolve();
      };
    
      request.onerror = function(e) {
        console.log(e.value);
        deferred.reject('Löschvorgang nicht angeschlossen.');
      };
    }
    
    return deferred.promise;
  };
  
  /////////////////////////  add ///////////////////////// 
  var addPerson = function(personText){
    var deferred = $q.defer();
    
    if(db === null){
      deferred.reject('IndexDB ist wurde nicht geöffnet.');
    }
    else{
      var trans = db.transaction(['person'], 'readwrite');
      var store = trans.objectStore('person');
      lastIndex++;
      var request = store.put({
        'id': lastIndex,
        'name': personText
      });
    
      request.onsuccess = function() {
        deferred.resolve();
      };
    
      request.onerror = function(e) {
        console.log(e.value);
        deferred.reject('Einfügen war nicht möglich.');
      };
    }
    return deferred.promise;
  };
  
  return {
    open: open,
    getPersonen: getPersonen,
    addPerson: addPerson,
    deletePerson: deletePerson
  };
  
});

