'use strict';

/**
 * @ngdoc function
 * @name firefoxOSShowcase.controller:GeolocationCtrl
 * @description
 * # GeolocationCtrl
 * Controller of the firefoxOSShowcase
 */
app.controller('GeolocationCtrl', function ($scope) {

  $scope.wpid = false;
  $scope.latitudeLabel = '';
  $scope.longitudeLabel = '';
  $scope.watchButtonText = 'Watch my position';
  $scope.myStyle = {'background': ''};
  $scope.showMap = true;

  $scope.getMyCurrentPosition = function () {

    $scope.longitudeLabel = '';

    if (!navigator.geolocation) {
      $scope.latitudeLabel = 'Geolocation is not supported!';
      return;
    }
    $scope.latitudeLabel = 'Locating your position... Please wait...';

    navigator.geolocation.getCurrentPosition(success, error);
  };

  var i = 0;

  $scope.watchMyPosition = function () {

    if ($scope.wpid) { // If watchPosition is activ and we already have a wpid which is the ID returned by navigator.geolocation.watchPosition()
      navigator.geolocation.clearWatch($scope.wpid);
      $scope.wpid = false;
      $scope.watchButtonText = 'Watch my position';
      $scope.myStyle = {'background': ''};
    }
    else {
      $scope.watchButtonText = 'Stop watching';
      $scope.myStyle = {'background': 'red'};

      $scope.longitudeLabel = '';

      if (!navigator.geolocation) {
        $scope.latitudeLabel = 'Geolocation is not supported!';
        return;
      }

      $scope.latitudeLabel = 'Locating your position... Please wait...';

      $scope.wpid = navigator.geolocation.watchPosition(success, error, {
        enableHighAccuracy: true, //Empfindlichkeits settings
        maximumAge: 30000,
        timeout: 27000
      });
    }
  };

  function success(position) {

    $scope.latitude = position.coords.latitude;
    $scope.longitude = position.coords.longitude;

    $scope.latitudeLabel = 'Latitude is ' + $scope.latitude + '°';
    $scope.longitudeLabel = 'Longitude is ' + $scope.longitude + '°';

    $scope.$broadcast('SHOW_MAP', $scope.latitude, $scope.longitude);

    //Using Google Static Maps API V2 to show current position
    //$scope.map = "http://maps.googleapis.com/maps/api/staticmap?center=" + $scope.latitude + "," + $scope.longitude + "&zoom=14&size=300x300&markers=color:0xF97C17%7C" + $scope.latitude + "," + $scope.longitude + "&sensor=true";
    //$scope.showMap = false;

    console.log((++i) + ': ' + $scope.longitude + ',' + $scope.latitude);
    $scope.$apply();
  }

  function error() {
    $scope.latitudeLabel = 'Unable to retrieve your location';
    $scope.longitudeLabel = '';

    navigator.geolocation.clearWatch($scope.wpid);
    $scope.wpid = false;
    $scope.watchButtonText = 'Watch my position';
    $scope.myStyle = {'background': ''};
    $scope.$apply();
  }
});
