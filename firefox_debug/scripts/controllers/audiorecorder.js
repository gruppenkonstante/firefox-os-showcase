'use strict';

/**
 * @ngdoc function
 * @name firefoxOSShowcase.controller:AudioRecCtrl
 * @description
 * # AudioRecCtrl
 * Controller of the firefoxOSShowcase audiorecorder
 */
app.controller('AudioRecCtrl', function ($scope, UserMediaService) {
        
    $scope.hideRecordButton = 'true';
    $scope.hasUserMediaService = UserMediaService.hasUserMedia;
        
    $scope.startRecord = function () {
            
        $scope.$broadcast('START_AUDIO_RECORDING');
        $scope.hideRecordButton = !$scope.hideRecordButton;
    };
        
    $scope.stopRecord = function () {
            
        $scope.$broadcast('STOP_AUDIO_RECORDING');
        $scope.hideRecordButton = !$scope.hideRecordButton;
    };
    
    console.log($scope);
});
