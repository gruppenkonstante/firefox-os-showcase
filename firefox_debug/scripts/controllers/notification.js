'use strict';

/**
 * @ngdoc function
 * @name firefoxOSShowcase.controller:NotificationCtrl
 * @description
 * # NotificationCtrl
 * Controller of the firefoxOSShowcase
 */
app.controller('NotificationCtrl', function ($scope) {
  var title = 'Notification';
  $scope.customNotification = 'Hello';
  
  $scope.showNotification = function() {
     // Let's check if the browser supports notifications
      if (!('Notification' in window)) {
        alert('This browser does not support desktop notification');
      }
    
      // Let's check whether notification permissions have alredy been granted
      else if (Notification.permission === 'granted') {
        var options = {
          body: $scope.customNotification
        };
        var notification = new Notification(title, options);
        console.log(notification);
      }
    
      // Otherwise, we need to ask the user for permission
      else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
          // If the user accepts, let's create a notification
          if (permission === 'granted') {
            var options = {
              body: $scope.customNotification
            };
            var notification = new Notification(title, options);
            console.log(notification);
          }
        });
      }
  };

  console.log($scope);
});