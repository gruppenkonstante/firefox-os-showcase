'use strict';

/**
 * @ngdoc overview
 * @name firefoxOSShowcase
 * @description
 * # App
 * AngularJS module for the whole application
 */
var app = angular.module('firefoxOSShowcase', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

app.config(function ($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'views/main.html',
		controller: 'MainCtrl'
	})
	.when('/actionmenu', {
		templateUrl: 'views/actionmenu.html',
		controller: 'ActionMenuCtrl'
	})
    .when('/blanko', {
        templateUrl: 'views/blanko.html',
        controller: 'BlankoCtrl'
    })
    .when('/audiorecorder', {
        templateUrl: 'views/audiorecorder.html',
        controller: 'AudioRecCtrl'
    })
    .when('/camera', {
        templateUrl: 'views/camera.html',
        controller: 'CameraCtrl'
    })
    .when('/geolocation', {
        templateUrl: 'views/geolocation.html',
        controller: 'GeolocationCtrl'
    })
    .when('/notification', {
        templateUrl: 'views/notification.html',
        controller: 'NotificationCtrl'
    })
    .when('/phonecall', {
        templateUrl: 'views/phonecall.html',
        controller: 'PhonecallCtrl'
    })
    .when('/indexeddb', {
        templateUrl: 'views/indexeddb.html',
        controller: 'indexedDBController'
    })
    .otherwise({
        redirectTo: '/'
    });
});
