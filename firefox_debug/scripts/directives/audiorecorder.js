'use strict';

app.directive('audiorecorder', ['UserMediaService', '$compile' , function (UserMediaService, $compile ) {
  return {
    template: '<div class="audiorecorder" ng-transclude></div>',
    restrict: 'E',
    replace: true,
    transclude: true,
    controller: function($scope){
        
        $scope.deleteRecordByID = function (clipID) {

           if (document.getElementById(clipID) !== null )  {
               console.log(clipID + ' is deleted'); 
               document.getElementById(clipID).remove();
           } 
      };
      
    },
    link: function postLink($scope, element) {
     	 var  audioStream = null,
            audioRecorder = null,
            isRecording = false;
            
       var onDestroy = function () {
        //Der Stream läuft mit dem bewilligen des Mikrozugriffs. Mit dieser Funktion wird er gestoppt
        if (!!audioStream && typeof audioStream.stop === 'function') {
          audioStream.stop();
        }
        console.log('Audiostream destroyed');
         
       };
       
       var onSuccess = function onSuccess(stream) {
          audioStream = stream;
          audioRecorder = new MediaRecorder(audioStream);
          
          //Starten der Aufnahme wenn isRecord = false
          if(!isRecording){
            console.log('Info: Recording started');
            audioRecorder.start();
            console.log(audioRecorder.state);
            isRecording = true;
            console.log(isRecording);
          }
       };   
       
       // called when any error happens
      var onFailure = function onFailure(err) {
        if (console && console.log) {
          console.log('The following error occured: ', err);
        }
        return;
        };
       
       
       var startAudioRecording = function () {
         
          // Check the availability of getUserMedia across supported browsers
          if (!UserMediaService.hasUserMedia) {
            onFailure({code:-1, msg: 'Browser does not support getUserMedia.'});
            return;
          }
          
          if (isRecording) {
            onFailure({code:-1, msg: 'Recording is already running.'});
            return;
          }

          var mediaConstraint = { video: false, audio: true };
          UserMediaService.getUserMedia(mediaConstraint, onSuccess, onFailure);     
          
       };     
       
       //Stoppen der Aufnahme
       var stopAudioRecording = function () {
           
          if(audioRecorder !== null && isRecording) {
            console.log('Info: Recording stopped');
            audioRecorder.stop();
            console.log(audioRecorder.state);
            isRecording = false;
           }
           
           audioRecorder.ondataavailable = function (e) {
               var random = Math.floor((Math.random() * 10000) + 1);            
               var clipName = prompt('Bitte geben Sie einen Namen für die Audiospur ein:', 'Titel1');
               var clipId =  random + '_' + clipName;
    	         
               if(!!clipName){
                                                              
                 var mediaElement = angular.element('<article id="'+ clipId +'" class="clip"></article>');

                 var title = angular.element('<p>'+ clipName +'</p>');
                 
                 var audio = angular.element('<audio style="width:100%"></audio>');
                 audio.attr('controls', '');
                 audio.attr('src', window.URL.createObjectURL(e.data));                   
                 
                 var button = angular.element('<button ng-click="deleteRecordByID(\'' + clipId +'\')">' + clipName +' löschen</button>');
                 button.addClass('danger');
                
                 mediaElement.append(title);
                 mediaElement.append(audio);
                 mediaElement.append(button);                                                    
                                 
                 element.append(mediaElement);
                 $compile(element.contents())($scope);
               }
          };                                    
       };      
      
      $scope.$on('$destroy', onDestroy);
      $scope.$on('START_AUDIO_RECORDING', startAudioRecording);
      $scope.$on('STOP_AUDIO_RECORDING', stopAudioRecording);   
      
    }
  };
}]);