'use strict';

app.directive('geolocation', function () {
  return {
    template: '<div ng-transclude></div>',
    restrict: 'E',
    replace: true,
    transclude: true,
    link: function postLink($scope, element){
      
      var showMap = function (event, latitude, longitude) {
        var width = element.parent()[0].offsetWidth,
            height = width;

        console.log('latitude ' + latitude + ', longitude ' + longitude);
        element.empty();

        //Using Google Static Maps API V2 to show current position
        var map = angular.element('<img src="http://maps.googleapis.com/maps/api/staticmap?center=' + 
          latitude +','+ longitude +'&zoom=14&size='+ width + 'x' + height + 
          '&markers=color:0xF97C17%7C' + latitude + ',' + longitude + '&sensor=true" />');
        element.append(map);
      };
      $scope.$on('SHOW_MAP', showMap);
    }
  };
});
