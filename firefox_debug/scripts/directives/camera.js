'use strict';

app.directive('camera', ['UserMediaService', function (UserMediaService) {
  return {
    template: '<div class="camera" ng-transclude></div>',
    restrict: 'E',
    replace: true,
    transclude: true,
    scope:
    {
      onError: '&',
      onStream: '&',
      onStreaming: '&',
      placeholder: '=',
      config: '=channel'
    },
    link: function postLink($scope, element) {
      var videoElem = null,
          videoStream = null,
          placeholder = null;

      $scope.config = $scope.config || {};

      var _removeDOMElement = function _removeDOMElement(DOMel) {
        if (DOMel) {
          angular.element(DOMel).remove();
        }
      };

      var onDestroy = function onDestroy() {
        if (!!videoStream && typeof videoStream.stop === 'function') {
          videoStream.stop();
        }
        if (!!videoElem) {
          delete videoElem.src;
        }
      };

      // called when camera stream is loaded
      var onSuccess = function onSuccess(stream) {
        videoStream = stream;

        // Firefox supports a src object
        if (UserMediaService.mozGetUserMedia) {
          videoElem.mozSrcObject = stream;
        } else {
          var vendorURL = window.URL || window.webkitURL;
          videoElem.src = vendorURL.createObjectURL(stream);
        }

        /* Start playing the video to show the stream from the camera */
        videoElem.play();
        $scope.config.video = videoElem;

        /* Call custom callback */
        if ($scope.onStream) {
          $scope.onStream({stream: stream});
        }
      };

      // called when any error happens
      var onFailure = function onFailure(err) {
        _removeDOMElement(placeholder);
        if (console && console.log) {
          console.log('The following error occured: ', err);
        }

        /* Call custom callback */
        if ($scope.onError) {
          $scope.onError({err:err});
        }

        return;
      };

      var startCamera = function (event, param) {
        console.log(param);
        videoElem = document.createElement('video');
        videoElem.setAttribute('class', 'camera-live');
        videoElem.setAttribute('autoplay', '');
        element.append(videoElem);

        if ($scope.placeholder) {
          placeholder = document.createElement('img');
          placeholder.setAttribute('class', 'camera-loader');
          placeholder.src = $scope.placeholder;
          element.append(placeholder);
        }

        // Default variables
        var isStreaming = false,
          //width = element.width = $scope.config.videoWidth || 320,
          width = element.parent()[0].offsetWidth,
          height = element.height = 0;
          
          if ($scope.config.videoWidth) {
              console.log($scope.config.videoWidth);
              width = $scope.config.videoWidth;
          }
          
          console.log('width: ' + width);

        // Check the availability of getUserMedia across supported browsers
        
        if (!UserMediaService.hasUserMedia) {
          onFailure({code:-1, msg: 'Browser does not support getUserMedia.'});
          return;
        }

        var mediaConstraint = { video: true, audio: true };
        UserMediaService.getUserMedia(mediaConstraint, onSuccess, onFailure);
  
        /* Start streaming the camera data when the video element can play
         * It will do it only once
         */
        videoElem.addEventListener('canplay', function() {
          if (!isStreaming) {
            var scale = width / videoElem.videoWidth;
            height = (videoElem.videoHeight * scale) ||
                      $scope.config.videoHeight;
            videoElem.setAttribute('width', width);
            videoElem.setAttribute('height', height);
            isStreaming = true;

            $scope.config.video = videoElem;

            _removeDOMElement(placeholder);

            /* Call custom callback */
            if ($scope.onStreaming) {
              $scope.onStreaming();
            }
          }
        }, false);
      };

      var stopCamera = function () {
        onDestroy();
        if(videoElem){
           videoElem.remove();
        }
       
      };
      
      $scope.$on('$destroy', onDestroy);
      $scope.$on('START_CAMERA', startCamera);
      $scope.$on('STOP_CAMERA', stopCamera);
    }
  };
}]);