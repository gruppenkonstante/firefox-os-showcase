'use strict';

/**
 * @ngdoc function
 * @name firefoxOSShowcase.service:UserMediaService
 * @description
 * Ist ein Service mit dem man abfragt ob getUserMedia verfügbar ist und 
 * gibt das entsprechende Objekt über zurück.
 * # UserMediaService
 * Service of the firefoxOSShowcase
 */

app.factory('UserMediaService', ['$window', function($window) {
  
  var hasUserMedia = function() {
    navigator.getUserMedia = ($window.navigator.getUserMedia || 
                              $window.navigator.webkitGetUserMedia ||
                              $window.navigator.mozGetUserMedia || 
                              $window.navigator.msGetUserMedia);
    return navigator.getUserMedia ? true : false;
  };
  
  var getUserMedia = function (mediaConstraint, onSuccess, onFailure) {
    return navigator.getUserMedia(mediaConstraint, onSuccess, onFailure);
  };

  var mozGetUserMedia = function() {
    return navigator.mozGetUserMedia;
  };

  return {
    hasUserMedia: hasUserMedia(),
    getUserMedia: getUserMedia,
    mozGetUserMedia: mozGetUserMedia
  };
}]);