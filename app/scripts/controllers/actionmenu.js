'use strict';

/**
 * @ngdoc function
 * @name <%= appName %>.controller:ActionMenuCtrl
 * @description
 * # ActionMenuCtrl
 * The controller for the action menu
 */
app.controller('ActionMenuCtrl', ['$scope', '$location', function($scope, $location) { 

    $scope.goNext = function (hash) { 
      $location.path(hash);
    };

    $scope.cancel = function() {
        window.history.back();
    };
	
	$scope.actionmenu = [{
        url: 'goNext("/blanko")',
        title: 'Blanko Page' 
    },{
        url: 'goNext("/audiorecorder")',
        title: 'Audiorecorder' 
    },{
        url: 'goNext("/camera")',
        title: 'Camera' 
    },{
        url: 'goNext("/geolocation")',
        title: 'Geolocation' 
    },{
        url: 'goNext("/notification")',
        title: 'Notification' 
    },{
        url: 'goNext("/phonecall")',
        title: 'Phone call/SMS' 
    },{
        url: 'goNext("/indexeddb")',
        title: 'IndexedDB' 
    },{
        url: 'goNext("/")',
        title: 'Main'   
    },{
        url: 'cancel()',
        title: 'Cancel' 
    }];
}]);