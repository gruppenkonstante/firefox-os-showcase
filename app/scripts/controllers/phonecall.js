'use strict';

/**
 * @ngdoc function
 * @name firefoxOSShowcase.controller:PhonecallCtrl
 * @description
 * # PhonecallCtrl
 * Controller of the firefoxOSShowcase
 */
app.controller('PhonecallCtrl', function ($scope) {

  $scope.phonenumber = '';

  $scope.callNumber = function () {
    var call = new MozActivity({
      name: 'dial',
      data: {
        number: $scope.phonenumber
      }
    });

    console.log(call);
  };

  $scope.sendSMS = function () {
    var sms = new MozActivity({
      name: 'new',
      data: {
        type: 'websms/sms',
        number: $scope.phonenumber
      }
    });

    console.log(sms);
  };
});
