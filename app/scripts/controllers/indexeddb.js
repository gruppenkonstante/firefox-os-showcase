'use strict';

/**
 * @ngdoc function
 * @name firefoxOSShowcase.controller:indexedDBController
 * @description
 * je nach funktion wird getPersonen, addPerson, oder deletePerson zurückgegeben
 * # indexedDBController
 * Controller of the firefoxOSShowcase
 */

app.controller('indexedDBController', function($window, indexedDBService){

  var indexedCtrl = this;
  indexedCtrl.Personen=[];
  
  indexedCtrl.refreshList = function(){
    indexedDBService.getPersonen().then(function(data){
      indexedCtrl.Personen=data;
    }, function(err){
      $window.alert(err);
    });
  };
  
  indexedCtrl.addPerson = function(){
    indexedDBService.addPerson(indexedCtrl.personText).then(function(){
      indexedCtrl.refreshList();
      indexedCtrl.personText='';
    }, function(err){
      $window.alert(err);
    });
  };
  
  indexedCtrl.deletePerson = function(id){
    indexedDBService.deletePerson(id).then(function(){
      indexedCtrl.refreshList();
    }, function(err){
      $window.alert(err);
    });
  };
  
  function init(){
    indexedDBService.open().then(function(){
      indexedCtrl.refreshList();
    });
  }
  
  init();
  
});