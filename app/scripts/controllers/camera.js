'use strict';

/**
 * @ngdoc function
 * @name firefoxOSShowcase.controller:CameraCtrl
 * @description
 * # CameraCtrl
 * Controller of the firefoxOSShowcase
 */
app.controller('CameraCtrl', function ($scope, UserMediaService) {
    
    $scope.hasUserMediaService = UserMediaService.hasUserMedia;
    
    //Prüft ob die Kamera an war
    $scope.cameraOn = false;

    $scope.$watch('cameraOn', function(){
   
        if ($scope.cameraOn) {
          
            console.log('Start Camera');
            //Greift auf die $scope.$on('START_WEBCAM', startWebcam); über den namen zu. Es können auch Parameter übergeben werden.
            $scope.$broadcast('START_CAMERA', 'StartCam über die Directive');
          
        } else {
          
          console.log('Stop Camera');
          $scope.$broadcast('STOP_CAMERA');
        }
        
        $scope.cameraState = $scope.cameraOn ? 'Stop' : 'Start';
    });
    
    console.log($scope);
});